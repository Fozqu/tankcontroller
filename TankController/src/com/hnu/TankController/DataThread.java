package com.hnu.TankController;

import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Foz on 14.11.2014.
 */
public class DataThread extends Thread {
   private  BluetoothSocket btSocket = null;
   private final OutputStream OutStream;


    public DataThread(BluetoothSocket socket){
        this.btSocket = socket;
        OutputStream tmpOut = null;
        try {
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {}
            OutStream = tmpOut;
    }

    public void sendData(Byte msg){
        byte BuffByte = msg.byteValue();
        try {
            OutStream.write(BuffByte);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
