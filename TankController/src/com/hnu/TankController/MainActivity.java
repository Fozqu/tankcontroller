package com.hnu.TankController;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.bluetooth.*;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.UUID;
/*
    0- правая гусеница
    1- левая гусеница UPD: ненормальное поведение
    2- обе в разные стороны
    3- левая гусеница назад
    4- правая гусеница вперед
    5-правая назад левая вперед
    6-правая вперед левая назад
    25 - nazad
    32 -vlevo
 */

public class MainActivity extends Activity {

     public static Integer i = 0;
    private static final int ENABLE_BT = 1;
    private Button mConnect,mDisconnect,mSend,mSend2,mStop,mSend3,mSend4;
    private TextView mText;
    private BluetoothAdapter btAdapter;
    private BluetoothSocket btSocket = null;
    private static String MAC_ADRESS = "00:11:08:26:06:42";
    private static UUID MY_UID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private DataThread MyThread = null;
    public static int mByte;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mText = (TextView) findViewById(R.id.text);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        mByte = 0;

        if (btAdapter != null){
            if (!btAdapter.isEnabled())
            {
                Intent enbaleBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enbaleBtIntent,ENABLE_BT);
            }
        }

        mConnect = (Button) findViewById(R.id.connect);
        mDisconnect = (Button) findViewById(R.id.disconnect);
        mSend =(Button) findViewById(R.id.send);
        mSend2 = (Button) findViewById(R.id.send2);
        mSend3 = (Button) findViewById(R.id.send3);
        mSend4 = (Button) findViewById(R.id.send4);
        mStop = (Button) findViewById(R.id.stop);
        mConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mText.setText(btDevice.getName());
                try{
                    btSocket.connect();
                    mText.setText("proffit");
                }
                catch (IOException e){
                    mText.setText("failed");
                }

            }
        });

        mDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    btSocket.close();
                    mText.setText("closed");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyThread.sendData((byte) 44);

            }
        });
        mByte = 130;
        mSend2.setOnClickListener(new View.OnClickListener() {
            @Override
            //32
            public void onClick(View v) {
                MyThread.sendData((byte) 32);
               // mText.setText(Integer.toString(mByte));
               // mByte ++;
            }
        });

        mSend3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyThread.sendData((byte) 72);
            }
        });

        mSend4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyThread.sendData((byte) 130);
            }
        });

        mStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyThread.sendData((byte) 80);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        final BluetoothDevice btDevice = btAdapter.getRemoteDevice(MAC_ADRESS);
        try {
            btSocket = btDevice.createRfcommSocketToServiceRecord(MY_UID);
        } catch (IOException e) {
            e.printStackTrace();
        }

        MyThread = new DataThread(btSocket);



    }
}
